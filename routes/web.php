<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontendController@index')->name('frontend.index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

//	Custom routes
	Route::resource('sales_people', 'SalePersonController');


	Route::resource('managers', 'ManagerController');
	Route::resource('pos', 'PosController');

	Route::post('possearch', 'PosController@search')->name('Search');

	Route::resource('townships', 'TownshipController');
    Route::resource('categories', 'CategoryController');
    Route::resource('item', 'ItemController');
    Route::resource('shop','ShopController');

    Route::post('sale', 'SaleController@store')->name('sale');
});


Route::middleware(['role:Sales'])->group(function () {

});

Route::middleware(['role:Manager'])->group(function () {

});


