@extends('layouts.app')

@section('content')

    <div class="header bg-gradient-primary pb-9 pt-5 pt-md-7">
    </div>

    <div class="row container-fluid">
        <div class="col-sm-7 mb-4 ml-5 mr-5 mt--7">



    <div class="row">
        <div class="col-6">
            <input type="text" class="form-control search" placeholder="Search or Scan" autofocus>
        </div>

        <div class="col-3">
            <button class="btn btn-success form-control btn_search">Search</button>
        </div>
        <div class="col-3">
            <button class="btn btn-warning form-control">Clear</button>
        </div>
    </div>



        </div>
        <div class="card shadow col-sm-7 mb-4 ml-5 mr-5 mt--3">
            <table class="table">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Item</th>
                    <th>Price</th>
                    <th>Qty.</th>
                    <th>Subtotal</th>
                </tr>
                </thead>

                <tbody id="item-list">

                </tbody>
            </table>
        </div>

        <div class="card shadow col-sm-3 mb-4 ml-5 mr-5 mt--7">
{{--            <div class="mt-3 ">--}}
{{--                <div class="row mb-4">--}}
{{--                    <div class="col-6"><h3>Subtotal :</h3></div>--}}
{{--                    <div class="offset-2"></div>--}}
{{--                    <div class="col-4"><h3>4000 MMK</h3></div>--}}
{{--                </div>--}}
{{--                <hr style="margin: 0;">--}}
{{--            </div>--}}

            <div class="mt-3">
                <div class="row mb-4">
                    <div class="col-6"><h3>Discount :</h3></div>
                    <div class="offset-2"></div>
                    <div class="col-4"><h3> - </h3></div>
                </div>
                <hr style="margin: 0;">
            </div>

            <div class="mt-3">
                <div class="row mb-4">
                    <div class="col-6"><h3 class="text-danger">Total :</h3></div>
                    <div class="offset-2"></div>
                    <div class="col-4"><h3 id="total"></h3></div>
                </div>
                <hr style="margin: 0;">
            </div>

            <div class="mt-2">
                <div class="row">
                    <div class="col-6 border-right">
                        <h2>Paid</h2> <br>
                        <input type="text" class="form-control search" id="paid-amount">
                    </div>



                    <div class="col-6">
                        <h2>Change</h2> <br>
                        <h1 class="text-center text-warning" id="change-money"></h1>
                    </div>
                </div>
            </div>
            <button class="btn btn-success mt-5 mb-4" id="end-sale">END SALE</button>
        </div>
    </div>

@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        showtable();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(".btn_search").click(function(){
        var item_code=$('.search').val();
        $.post("{{route('Search')}}",{item_code:item_code},function(response){
            // console.log(response.name);

            var id = response.id;
            var name = response.name;
            var price = response.sale_price;
            var item_code = response.item_code;

            var item = {id:id, item_code:item_code, name:name, price:price, qty:1};

            // console.log(item);

            var mycart = localStorage.getItem('cart');

            if(!mycart)
            {
                mycart = '{"itemlist": []}';
            }

            var mycartobj = JSON.parse(mycart);

            var hasid = false;

            $.each(mycartobj.itemlist, function(i,v){
              if(v.id == id)
              {
                  hasid = true;
                  v.qty++;
              }

            });

              if(!hasid)
              {
                  mycartobj.itemlist.push(item);
              }

            localStorage.setItem('cart', JSON.stringify(mycartobj));
    showtable();
        })
  });

        function showtable() {
            var mycartobj = localStorage.getItem('cart');
            // console.log("showtable: " + mycartobj);

            if(mycartobj){
                var itemArray = JSON.parse(mycartobj);
                // console.log(itemArray);
                var html = '';
                var html1 = '';


                var no = 1 ;
                var total = 0 ;

                $.each(itemArray.itemlist, function(k,v){
                    var name = v.name;
                    var price = v.price;
                    var qty = v.qty;
                    var id = v.id;
                    var subtotal = price * qty;
                     total += subtotal;

                    html += `
                        <tr>
                            <td>${no++}</td>
                            <td>${name}</td>
                            <td>${price}</td>
                            <td><button class="decrese" data-qty=${qty} data-id=${k}>-</button>   ${qty}  <button class="increse" data-qty=${qty} data-id=${k}>+</button></td>
                            <td>${subtotal}</td>
<!--                            <td><i class="far fa-trash-alt clear"></i></td>-->
                        </tr>
                    `

                    html1 = `
                        ${total}
                    `
                });
                $("#item-list").html(html);
                $("#total").html(html1);
            }
            else {
                $("#item-list").html('');
                $("#total").html('');
            }
        }

        $("#item-list").on('click','.decrese',function(){
            var id=$(this).data('id');
            // console.log(id);
            var stuString=localStorage.getItem("cart");
            if(stuString){
                var stuArray=JSON.parse(stuString);
                $.each(stuArray.itemlist,function(i,v){
                    if(i==id) {

                        var qty1 = v.qty;
                        qty1--;
                        stuArray.itemlist[id].qty = qty1;
                        // console.log(v);

                        if (v.qty == 0) {
                            // console.log(v);
                            stuArray.itemlist.splice(id, 1);
                        }

                    }
                })
                var stuData=JSON.stringify(stuArray);
                localStorage.setItem("cart",stuData);
            }
            showtable();
        })




        $("#item-list").on('click','.increse',function(){
            var id=$(this).data('id');
            var qty=$(this).data('qty');
            // console.log(qty);

            var stuString=localStorage.getItem("cart");

            if(stuString){
                var stuArray=JSON.parse(stuString);
                $.each(stuArray.itemlist,function(i,v){
                    if(i==id){
                        var qty1=v.qty;
                        // console.log(qty1);
                        qty1++;

                        // console.log(qty1);
                        stuArray.itemlist[id].qty=qty1;
                    }

                })
                var stuData=JSON.stringify(stuArray);
                localStorage.setItem("cart",stuData);
            }
           showtable();
        })

        $('#paid-amount').change(function () {
            var paidAmount = $(this).val();
            var total = $('#total').text();
        // alert(total);
            var changeAmount = paidAmount - total;
            $('#change-money').text(changeAmount);

        })

        $("#end-sale").click(function () {
            var itemString=localStorage.getItem("cart");
            var itemobj=JSON.parse(itemString);
            var total = $('#total').text();

            var itemArray = itemobj.itemlist;

            // console.log(itemArray);

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.post("{{route('sale')}}",{itemArray:itemArray, total:total},function (response) {
                console.log(response);
                if(response == "success"){
                    localStorage.clear();
                    showtable();
                    alert("Success Sale , Make another Sale !")
                }

            })

        })
    });
</script>


@endsection
