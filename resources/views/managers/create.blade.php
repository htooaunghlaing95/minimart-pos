@extends('layouts.app')

@section('content')
    {{-- Header Background--}}
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
    </div>
    <div class="card shadow mb-4 ml-5 mr-5 mt--7">
        <div class="card-header">
            <h2>Create New Manager</h2>
        </div>

        <div class="card-body">
            <form method="post" action="{{route('managers.store')}}">
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-sm-1 col-form-label">Name :</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="name" id="name" aria-describedby="name">
                        @error('name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>


                    <label for="email" class="col-sm-1 col-form-label">Email :</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="email" id="email" aria-describedby="email">
                        @error('email')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-5">

                        <label for="shop_id" class="col-form-label d-inline-block">Shop</label>
                        <select class="form-control" name="shop_id">

                            @foreach ($shops as $row)

                            <option value="{{$row->id}}">{{$row->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="phone" class="col-sm-1 col-form-label">Phone :</label>
                    <div class="col-sm-5">
                        <input type="phone" class="form-control" name="phone" id="phone" aria-describedby="phone">
                        @error('phone')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <label for="email" class="col-sm-1 col-form-label">Address:</label>
                    <div class="col-sm-5">
                        <textarea class="form-control" name="address" id="address" rows="2"></textarea>
                        @error('email')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <button type="submit" class="btn btn-primary mb-2">Save</button>

            </form>
        </div>
    </div>

@endsection
