<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Home</title>
	<link rel="icon" href="img/Fevicon.png" type="image/png">

  <link rel="stylesheet" href="{{asset('frontendtemplate/vendors/bootstrap/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('frontendtemplate/vendors/fontawesome/css/all.min.css')}}">
  <link rel="stylesheet" href="{{asset('frontendtemplate/vendors/themify-icons/themify-icons.css')}}">
  <link rel="stylesheet" href="{{asset('frontendtemplate/vendors/linericon/style.css')}}">
  <link rel="stylesheet" href="{{asset('frontendtemplate/vendors/owl-carousel/owl.theme.default.min.css')}}">
  <link rel="stylesheet" href="{{asset('frontendtemplate/vendors/owl-carousel/owl.carousel.min.css')}}">
  <link rel="stylesheet" href="{{asset('frontendtemplate/css/magnific-popup.css')}}">
  <link rel="stylesheet" href="{{asset('frontendtemplate/vendors/flat-icon/font/flaticon.css')}}">

  <link rel="stylesheet" href="{{asset('frontendtemplate/css/style.css')}}">

    <link rel="stylesheet" href="{{asset('css/frontend.css')}}">


    <link href="https://fonts.googleapis.com/css?family=Paytone+One|Scada&display=swap" rel="stylesheet">


</head>



<body>

  <!--================ Header Menu Area start =================-->
  <header class="header_area bg-light" >
    <div class="main_menu">
      <nav class="navbar navbar-expand-lg navbar-light">

        <div class="container box_1620">
{{--          <a class="navbar-brand logo_h" href="index.html"><img src="img/logo.png" alt=""></a>--}}
            <a class="navbar-brand" href="#"> <img src="{{asset('image/logo.png')}}" class="img-fluid" style="width:90px; height: auto;"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

          <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
            <ul class="nav navbar-nav menu_nav justify-content-end">
              <li class="nav-item active"><a class="nav-link" href="#home">Home</a></li>
              <li class="nav-item"><a class="nav-link" href="#features">About</a></li>
              <li class="nav-item"><a class="nav-link" href="#functions">Function</a>
                <li class="nav-item"><a class="nav-link" href="#pricing">Price & Services</a>
            </ul>

            <ul class="nav-right text-center text-lg-right py-4 py-lg-0">
              <li><a href="{{route('login')}}">Member Login</a></li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  </header>
  <!--================Header Menu Area =================-->
  <body>
  	@yield('content')
  </body>


  <!-- ================ start footer Area ================= -->
  <footer class="footer-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-3  col-md-6 col-sm-12">
          <div class="single-footer-widget">
            <h6>About Mini POS</h6>
            <p>
              The tiny web app that help mini mart owner to become comfortable life and stress free people. Our app can help the owners' days colorful.
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12">
          <div class="single-footer-widget">
            <h6>Navigation Links</h6>
            <div class="row">
              <div class="col">
                <ul>
                  <li><a href="#">Home</a></li>
                  <li><a href="#">Function</a></li>
                  <li><a href="#">Price & Services</a>
                    <li><a href="#">Career</a></li>
                </ul>
              </div>

            </div>
          </div>
        </div>
        <div class="col-lg-3  col-md-6 col-sm-12">
          <div class="single-footer-widget">
            <h6>Newsletter</h6>
            <p>
              You want our stuffs to call and explain you more about our app?
            </p>
            <div id="mc_embed_signup">
              <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="subscription relative">
                <div class="input-group d-flex flex-row">
                  <input name="EMAIL" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '" required="" type="email">
                  <button class="btn bb-btn"><span class="lnr lnr-location"></span></button>
                </div>
                <div class="mt-10 info"></div>
              </form>
            </div>
          </div>
        </div>

      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row align-items-center">
            <p class="col-lg-8 col-sm-12 footer-text m-0 text-center text-lg-left">
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | TheCoder(Team-2) | MIC Bootcamp</p>

            <div class="col-lg-4 col-sm-12 footer-social text-center text-lg-left">
            <a href="#"><i class="fab fa-facebook-f"></i></a>
            <a href="#"><i class="fab fa-twitter"></i></a>
            <a href="#"><i class="fab fa-dribbble"></i></a>
            <a href="#"><i class="fab fa-behance"></i></a>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- ================ End footer Area ================= -->




  <script src="{{asset('frontendtemplate/vendors/jquery/jquery-3.2.1.min.js')}}"></script>
  <script src="{{asset('frontendtemplate/vendors/bootstrap/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('frontendtemplate/vendors/owl-carousel/owl.carousel.min.js')}}"></script>
  <script src="{{asset('frontendtemplate/vendors/Magnific-Popup/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{asset('frontendtemplate/js/jquery.ajaxchimp.min.js')}}"></script>
  <script src="{{asset('frontendtemplate/js/mail-script.js')}}"></script>
  <script src="{{asset('frontendtemplate/js/countdown.js')}}"></script>
  <script src="{{asset('frontendtemplate/js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{asset('frontendtemplate/js/main.js')}}"></script>



</body>
</html>
