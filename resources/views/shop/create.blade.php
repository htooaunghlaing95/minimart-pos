@extends('layouts.app')

@section('content')
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
</div>
<div class="card shadow mb-4 ml-5 mr-5 mt--7">
	<div class="card-header">
		<h2>Create New Shop</h2>
	</div>

	<div class="card-body">
		<form action="{{route('shop.store')}}" method="POST">
			@csrf
			<h2>Shop Information:</h2>
			
			<div class="form-group row">
				<div class="col-sm-5">
					<label for="name" >Shop Name</label>
					<input type="text" class="form-control" id="name" name="name" placeholder="">
				</div>
			</div>
				<div class="form-group row">
					<div class="col-sm-5">

						<label for="township" class="col-form-label d-inline-block">Township</label>
						<select class="form-control" name="township_id">

							@foreach ($township as $row)
	  						<option value="{{$row->id}}">{{$row->name}}</option>
	  						@endforeach
						</select>
					</div>

				</div>
			<div class="form-group row">
				<div class="col-sm-5">
					<label for="info" >Info</label>
					<input type="text" class="form-control" id="info" name="info" placeholder="">
				</div>
			</div>

			<button type="submit" class="btn btn-primary ">save</button>

		</form>
	</div>
</div>
@endsection
