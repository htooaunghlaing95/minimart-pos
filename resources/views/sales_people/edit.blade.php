@extends('layouts.app')

@section('content')
    {{-- Header Background--}}
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
    </div>
    <div class="card shadow mb-4 ml-5 mr-5 mt--7">
        <div class="card-header">
            <h2>Create New Sales Person</h2>
        </div>

        <div class="card-body">
            <form method="post" action="{{route('sales_people.update', $user->id)}}">
                @csrf
                @method('PUT')

                <div class="form-group row">
                    <label for="name" class="col-sm-1 col-form-label">Name :</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="name" id="name" aria-describedby="name" value="{{$user->name}}">
                        @error('name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <label for="email" class="col-sm-1 col-form-label">Email :</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="email" id="email" aria-describedby="email" value="{{$user->email}}">
                        @error('email')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="phone" class="col-sm-1 col-form-label">Phone :</label>
                    <div class="col-sm-5">
                        <input type="phone" class="form-control" name="phone" id="phone" aria-describedby="phone" value="{{$user->salePerson->phone}}">
                        @error('phone')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <label for="email" class="col-sm-1 col-form-label">Address:</label>
                    <div class="col-sm-5">
                        <textarea class="form-control" name="address" id="address" rows="2">{{$user->salePerson->address}}</textarea>
                        @error('email')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <label for="national_id_no" class="col-sm-1 col-form-label">ID Card No. :</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="national_id_no" id="national_id_no" aria-describedby="national_id_no" value="{{$user->salePerson->national_id_no}}">
                        @error('national_id_no')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

{{--                    <label for="password" class="col-sm-1 col-form-label">Password :</label>--}}
{{--                    <div class="col-sm-5">--}}
{{--                        <input type="password" class="form-control" name="password" id="password" aria-describedby="password">--}}
{{--                        @error('email')--}}
{{--                        <span class="text-danger">{{ $message }}</span>--}}
{{--                        @enderror--}}
{{--                    </div>--}}
                </div>

                <button type="submit" class="btn btn-primary mb-2">Update</button>

            </form>
        </div>
    </div>

@endsection
