@extends('layouts.app')

@section('content')
    {{-- Header Background--}}
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
    </div>
    <div class="card shadow mb-4 ml-5 mr-5 mt--7">
        <div class="card-header">
            <h2 class="d-inline-block">Sales Person Information</h2>
        </div>

        <div class="card-body">
            <form>
                @csrf
                @method('PUT')

                <div class="form-group row">
                    <label for="name" class="col-sm-1 col-form-label">Name :</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="name" id="name" aria-describedby="name" value="{{$user->name}}" disabled>
                    </div>

                    <label for="email" class="col-sm-1 col-form-label">Email :</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="email" id="email" aria-describedby="email" value="{{$user->email}}" disabled>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="phone" class="col-sm-1 col-form-label">Phone :</label>
                    <div class="col-sm-5">
                        <input type="phone" class="form-control" name="phone" id="phone" aria-describedby="phone" value="{{$user->salePerson->phone}}" disabled>
                    </div>

                    <label for="email" class="col-sm-1 col-form-label">Address:</label>
                    <div class="col-sm-5">
                        <textarea class="form-control" name="address" id="address" rows="2" disabled>{{$user->salePerson->address}}</textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="national_id_no" class="col-sm-1 col-form-label">ID Card No. :</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="national_id_no" id="national_id_no" aria-describedby="national_id_no" value="{{$user->salePerson->national_id_no}}" disabled>
                    </div>
                </div>

                <a href="{{route('sales_people.index')}}" class="btn btn-primary">Back</a>

            </form>
        </div>
    </div>

@endsection
