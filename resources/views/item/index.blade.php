@extends('layouts.app')

@section('content')
    {{-- Header Background--}}
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
    </div>
    <div class="card shadow mb-4 ml-5 mr-5 mt--7">
        <div class="card-header">
            <h2 class="d-inline-block">Item Listings</h2>
            <a href="{{route('item.create')}}" class="btn btn-success float-right"><i class="fas fa-plus"></i> Add New </a>
        </div>

        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Item Code</th>
                        <th>Category</th>
                        <th>Sale Price</th>
                        <th>Quantity</th>
                        <th>Function</th>
                    </tr>
                </thead>

                <tbody>
                    @php
                        $i = 1;
                    @endphp

                @foreach($items as $item)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->item_code}}</td>
                        <td>{{$item->category->name}}</td>
                        <td>{{$item->sale_price}}</td>
                        <td>{{$item->quantity}}</td>
                        <td>
                            <a href="{{route('item.edit', $item->id)}}" class="btn btn-warning"><i class="far fa-edit"></i>Edit</a>
                            <a href="javascript:;" data-toggle="modal" onclick="deleteData({{$item->id}})"
                               data-target="#DeleteModal" class="btn btn-xs btn-danger">
                                <i class="far fa-trash-alt"></i> Delete</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
       <div id="DeleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog ">
            <!-- Modal content-->
            <form action="" id="deleteForm" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title">Delete Comfirmation</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @csrf
                        @method('DELETE')
                        <p class="text-center">Are You Sure Want To Delete ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-danger" data-dismiss="modal" onclick="formSubmit()">Delete</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    {{-- Modal End --}}

    {{-- JS - Delete Modal --}}
    <script type="text/javascript">
        function deleteData(id)
        {
            var id = id;
            var url = '{{ route("item.destroy", ":id") }}';
            url = url.replace(':id', id);
            $("#deleteForm").attr('action', url);
        }

        function formSubmit()
        {
            $("#deleteForm").submit();
        }
    </script>
    @endsection


