@extends('layouts.app')

@section('content')
    {{-- Header Background--}}
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
    </div>
    <div class="card shadow mb-4 ml-5 mr-5 mt--7">
        <div class="card-header">
            <h2>Create Items</h2>
        </div>
        <form method="POST" action="{{route('item.store')}}">
          <div class="container">
        	@csrf

  <div class="form-group row">
          <div class="col-sm-12">

            <label for="category_id" class="col-form-label d-inline-block">Category</label>
            <select class="form-control" name="category_id">

              @foreach ($category as $row)
                <option value="{{$row->id}}">{{$row->name}}</option>
                @endforeach
            </select>
          </div>

        </div>

  <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" id="name" name="name" >
  </div>
  <div class="form-group">
    <label for="initial_price">Initial Price</label>
    <input type="text" class="form-control" id="initial_price" name="initial_price">
  </div>
  <div class="form-group">
    <label for="sale_price">Sale price</label>
    <input type="text" class="form-control" id="sale_price" name="sale_price">
  </div>
  <div class="form-group">
    <label for="quantity">Quantity</label>
    <input type="text" class="form-control" id="quantity" name="quantity">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>

    </div>
@endsection
