@extends('layouts.app')

@section('content')
    {{-- Header Background--}}
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
    </div>
    <div class="card shadow mb-4 ml-5 mr-5 mt--7">
        <div class="card-header">
            <h2>Create Items</h2>
        </div>
        <form method="POST"  action="{{route('item.update', $item->id)}}">
          <div class="container">
        	@csrf
          @method('PUT')
   <div class="form-group row">
          <div class="col-sm-12">

            <label for="category_id" class="col-form-label d-inline-block">Category</label>
            <select class="form-control" name="category_id">

              @foreach ($category as $row)
                <option value="{{$row->id}}">{{$row->name}}</option>
                @endforeach
            </select>
          </div>

        </div>
  <div class="form-group" >
    <label for="item_code">Item Code</label>
    <input type="text" class="form-control" id="item_code" name="item_code" value="{{$item->item_code}}"  >
  </div>
  <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" id="name" name="name" value="{{$item->name}}" >
  </div>

  <div class="form-group">
    <label for="sale_price">Sale price</label>
    <input type="text" class="form-control" id="sale_price" name="sale_price" value="{{$item->sale_price}}" >
  </div>
  <div class="form-group">
    <label for="quantity">Quantity</label>
    <input type="text" class="form-control" id="quantity" name="quantity" value="{{$item->quantity}}" >
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>

    </div>
@endsection