@extends('layouts.app')

@section('content')
    {{-- Header Background--}}
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
    </div>
    <div class="card shadow mb-4 ml-5 mr-5 mt--7">
        <div class="card-header">
            <h2>Create New Category</h2>
        </div>

        <div class="card-body">
            <form method="post" action="{{route('categories.update', $category->id)}}">
                @csrf
                @method('PUT')

                <div class="form-group row" >
                    <label for="name" class="col-sm-1 col-form-label">Name :</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="name" id="name" aria-describedby="name" value="{{$category->name}}">
                        @error('name')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
              
                    <button type="submit" class="btn btn-primary ">Update</button>
                      </div>
                </div>

                

            </form>
        </div>
    </div>

@endsection
