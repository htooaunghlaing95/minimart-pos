@extends('layouts.app')

@section('content')
  <div class="header bg-gradient-primary pb-8 pt-5 pt-md-7">
    </div>
    <div class="card shadow mb-4 ml-5 mr-5 mt--7">
        <div class="card-header">
            <h2>Create New Category</h2>
        </div>

        <div class="card-body">
           <form action="{{route('categories.store')}}" method="POST">
            @csrf
                <div class="form-group row">
                    <div class="col-sm-5">
                        <label for="name" class=""> Name </label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="">
                    </div>
                </div>

                <button type="submit" class="btn btn-primary ">save</button>
            </form>
        </div>
    </div>

@endsection
