@extends('hometemplate')
@section('content')

    <div class="container-fluid">

    {{--  BANNER SECTION --}}
        <section id="home">
        <div class="row">
            <div class="col-lg-6 col-sm-12 col-md-12">
                <h1 class="display-2 mt-5">Your Shop Partner</h1>
                <p class="display-4 ml-5 pt-4">Start using our mini-mart Point of sale web application to grow your business.</p>
            </div>

            <div class="col-lg-6 col-md-12 col-md-12">
                <img class="image" src="{{asset('image/banner-1.jpg')}}" style="width: 100%; height: 100%;">
            </div>
        </div>
        </section>
    {{--  BANNER SECTION END --}}

    {{-- FEATURES SECTION --}}
        <section id="features">
            <div class="row feature-section">
                <div class="col-lg-6 col-sm-12 col-md-12">
                    <img class="image" src="{{asset('image/features.png')}}" style="width: 80%; height: 100%;">
                </div>

                <div class="col-lg-6 col-md-12 col-md-12">
                    <h2 class="display-3 title-white" id="">How our web app can help you!</h2>
                    <h3 id="feature-description"> We provide a single dashboard to gain real insights into what is happening in your store. <br>
                        You can manage your store items from our Web App. <br>
                        Add items, get reports and track your sale people performance.</h3>
                </div>
            </div>
        </section>
    {{-- FEATURES SECTION END--}}

    {{-- FUNCTION SECTION--}}
        <div class="container">
            <div>
                <h2 class="display-4 text-center mt-5" id="function-title">Rich Function to <br> Empower Your Store</h2>
            </div>
        </div>

        <div class="container mb-4" id="function-section">
            <section id="functions">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12" style="text-align: center; padding-top: 8%;">
                            <a href="#" style="text-align: center;"><i class="fas fa-10x fa-tachometer-alt function-icon"></i></a>
                            <h3 style="color: #1BA4C6;">Dashboard</h3>
                            <p class="pt-4">The main area that you will love to see.<br> From there you can go to all your features pages.</p>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12" style="text-align: center; padding-top: 8%;">
                        <a href="#" style="text-align: center;"><i class="far fa-10x fa-chart-bar function-icon"></i></a>
                        <h3 style="color: #1BA4C6;">Reports</h3>
                        <p class="pt-4">Monthly, Daily reports. <br> Where you can see your store progress.</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12" style="text-align: center; padding-top: 8%;">
                        <a href="#" style="text-align: center;"><i class="fas fa-10x fa-clipboard-list function-icon"></i></a>
                        <h3 style="color: #1BA4C6;">Bills</h3>
                        <p class="pt-4">Recorded your sale lists so that you can see <br> how much profit your store did in the dashboard</p>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-12" style="text-align: center; padding-top: 8%;">
                        <a href="#" style="text-align: center;"><i class="fas fa-10x fa-users function-icon"></i></a>
                        <h3 style="color: #1BA4C6;">Sales Person</h3>
                        <p class="pt-4">If you have sales people, you can add themes <br> to use the system and see their progress too.</p>
                    </div>
                </div>
            </section>
        </div>
    {{-- FUNCTION SECTION END --}}
    </div>

        <div class="container-fluid price-background">
            <section class="pricing py-5">
                <h2 id="price-title" class="display-3 mb-5">Price & Services</h2>
                <div class="container">
                    <div class="row">
                        <!-- Free Tier -->
                        <div class="col-lg-4">
                            <div class="card mb-5 mb-lg-0">
                                <div class="card-body">
                                    <h5 class="card-title text-muted text-uppercase text-center">Mini</h5>
                                    <h6 class="card-price text-center">$0<span class="period">/month</span></h6>
                                    <hr>
                                    <ul class="fa-ul">
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>1 Admin & User</li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>300 Items</li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>50 Categories</li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>1 Store</li>
                                        <li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Unlimited Daily Reports</li>
                                        <li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Unlimited Monthly Reports</li>
                                        <li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Free Service Care</li>
                                        <li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>In House System Tutor</li>
                                    </ul>
                                    <a href="#" class="btn btn-block btn-primary text-uppercase">Buy Now</a>
                                </div>
                            </div>
                        </div>
                        <!-- Plus Tier -->
                        <div class="col-lg-4">
                            <div class="card mb-5 mb-lg-0">
                                <div class="card-body">
                                    <h5 class="card-title text-muted text-uppercase text-center">Medium</h5>
                                    <h6 class="card-price text-center">$9<span class="period">/month</span></h6>
                                    <hr>
                                    <ul class="fa-ul">
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span><strong>4 Admins & Users</strong></li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>1,000 Items</li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>100 Categories</li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>4 Store</li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Daily Reports</li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Monthly Reports</li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>Free Service Care</li>
                                        <li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>In House System Tutor</li>
                                    </ul>
                                    <a href="#" class="btn btn-block btn-primary text-uppercase">Buy Now</a>
                                </div>
                            </div>
                        </div>
                        <!-- Pro Tier -->
                        <div class="col-lg-4">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title text-muted text-uppercase text-center">Large</h5>
                                    <h6 class="card-price text-center">$49<span class="period">/month</span></h6>
                                    <hr>
                                    <ul class="fa-ul">
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span><strong>Unlimited Users</strong></li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Items</li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Categories</li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Store</li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Daily Reports</li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Monthly Reports</li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>Free Service Care</li>
                                        <li><span class="fa-li"><i class="fas fa-check"></i></span>In House System Tutor</li>
                                    </ul>
                                    <a href="#" class="btn btn-block btn-primary text-uppercase">Buy Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>


@endsection
