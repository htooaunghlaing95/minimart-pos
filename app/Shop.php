<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

      public function manager()
    {
        return $this->hasMany('App\Manager');

    }

    public function township()
    {
        return $this->belongsTo('App\Township');
    }
    public function categories()
    {
        return $this->hasMany('App\Category');
    }


    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'info'];
}
