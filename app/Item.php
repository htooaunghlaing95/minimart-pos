<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Item extends Model
{
	use SoftDeletes;

    protected $fillable=['item_code','name','initial_price','sale_price','quantity'];

    protected $dates = ['deleted_at'];

    public function category()
     {
     	return $this->belongsTo('App\Category');
     }

}
