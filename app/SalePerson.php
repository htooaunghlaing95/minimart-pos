<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SalePerson extends Model
{
    use SoftDeletes;

    protected $fillable = ['phone', 'address', 'national_id_no'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    protected $dates = ['deleted_at'];

}
