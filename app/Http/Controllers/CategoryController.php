<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Auth;
use App\Shop;
use App\User;
use App\Manager;
use Illuminate\Support\Facades\DB;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('role:Manager')->except('index');
    }

    public function index()
    {
        $category = DB::table('categories')
                    ->where('user_id','=',Auth::id())
                    ->select('*')
                     ->get();
       // $category = Category::all();
        return view ('categories.index', compact('category'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('categories.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
         $request->validate([
           "name" => "required",
             ]);

//         dd(Auth::user()->manager);
         $shop_id = Auth::user()->manager->shop->id;
         //dd($shop_id);
          $category = New Category;
          $category->shop_id = $shop_id;
          $category->user_id = Auth::id();

          $length = count(Shop::find($shop_id)->categories);

          if ($length > 0){
              $category -> code_no =++ Shop::find($shop_id)->categories[$length-1]->code_no;
          }else{
              $category -> code_no = $shop_id . '0';
          }

          $category->name = request('name');
          $category -> save();

           return redirect()->route('categories.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = category::find($id);
        return view('categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            "name" => 'required | min:5 | max: 100'
             ]);

        $category = Category::find($id);

        $category->name = request('name');

          $category -> save();

        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category -> delete();

        return redirect()->route('categories.index');
    }
}
