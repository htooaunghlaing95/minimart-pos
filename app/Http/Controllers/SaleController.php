<?php

namespace App\Http\Controllers;

use App\Sale;
use App\SaleDetail;
use Illuminate\Http\Request;
use Auth;

class SaleController extends Controller
{
    public function store(Request $request)
    {
        $itemArray = request('itemArray');
        $user_id = Auth::id();
        $voucher_no = uniqid();
        $date = date('y-m-d');
        $total = request('total');


        $order = new Sale;

        $order -> user_id = $user_id;
        $order -> voucher_no = $voucher_no;
        $order -> date = $date;
        $order -> total = $total;

        $order->save();

        foreach ($itemArray as $row) {

            $item_code = $row['item_code'];
            $voucher_no = $order->voucher_no;
            $quantity = $row['qty'];

            $saleDetails = new SaleDetail;

            $saleDetails -> item_code = $item_code;
            $saleDetails -> voucher_no = $voucher_no;
            $saleDetails -> quantity = $quantity;

            $saleDetails -> save();
            return ("success");

        }

    }
}
