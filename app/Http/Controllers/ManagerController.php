<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Manager;
use App\User;
use App\Shop;
use Auth;
use Illuminate\Support\Facades\Hash;

class ManagerController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:Admin');
    }


    public function index()
    {
        $managers = Manager::all();
        //dd($managers);
        return view('managers.index', compact('managers'));
    }

    public function create()
    {
        $manager = Manager::all();
         $shops = Shop::all();
        return view('managers.create', compact('manager','shops'));
    }

    public function store(Request $request)
    {
        $request->validate([
            "name" => "required",
            "email" => "required | unique:users",
            "phone" => "required",
            "address" => "required"
        ]);

        $user = New User;

        $user -> name = request('name');
        $user -> email = request('email');
        $user -> password = Hash::make('1234567890');
        $user -> save();
        $user->assignRole('Manager');

        $manager = new Manager();

        $manager -> user_id = $user->id;
        $manager -> shop_id = request('shop_id');
         // Fixed shop_id for now !
        $manager -> phone = request('phone');
        $manager -> address = request('address');

        $manager -> save();

        return redirect()->route('managers.index');
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('managers.show', compact('user'));
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('managers.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $user = User::where('id', $id)->update([
            'name' => $request->name,
            'email' => $request->email
        ]);

        $managers = Manager::where('user_id', $id)->update([
            'phone' => $request->phone,
            'address' => $request->address
        ]);

        return redirect()->route('managers.index');
    }

    public function destroy($id)
    {

        Manager::where('user_id',$id)->delete();
        User::find($id)->delete();

        return redirect()->route('managers.index');
    }
}
