<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use App\Township;
use App\Manager;
use App\User;
use Auth;
use Illuminate\Support\Facades\DB;


class ShopController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:Admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shops = Shop::all();

        return view('shop.index', compact('shops'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $township = Township::all();
        return view('shop.create', compact('township'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     "user_id"=> "required",
        //     "name"=> "required",
        //     "township_id"=> "required",


        // ]);

        $shop=New Shop;
        $shop ->name=request('name');
        $shop ->township_id=request('township_id');
        $shop ->info=request('info');
        $shop -> save();


        return redirect()->route('shop.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $manager=Manager::all();
        $township = Township::all();
        $shop = Shop::find($id);
        return view('shop.edit', compact('shop','township','manager'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $request->validate([
        //     "user_id"=> "required",
        //     "name"=> "required",
        //     "township_id"=> "required",


        // ]);

        $shop = Shop::find($id);

//        $shop -> user_id=Auth::id();
        $shop -> name=request('name');
        $shop -> township_id=request('township_id');
        $shop -> info=request('info');
        $shop -> save();

        return redirect()->route('shop.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $shop = Shop::find($id);
        $shop -> delete();

        return redirect()->route('shop.index');
    }
}
