<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Item;
use App\Category;
use App\Shop;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:Manager');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::where('user_id','=',Auth::id())->get();
        return view ('item.index', compact('items'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = DB::table('categories')
                    ->where('user_id','=',Auth::id())
                    ->select('*')
                     ->get();

        return view('item.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        $request->validate([
//            "user_id"=>"required",
//           "category_id" => "required",
//            "name" => "required ",
//            "initial_price" => "required",
//            "sale_price" => "required",
//            "quantity" => "required"
//        ]);


        $item=New Item;
        $item ->user_id = Auth::id();
        $item->category_id = request('category_id');
        $category = Category::find(request('category_id'));
        $category_code = $category->code_no;
        if (count(Item::all())>0 && Item::orderBy('id', 'desc')->first()->category_id == $category->id){
            $item->item_code =++Item::orderBy('id', 'desc')->first()->item_code;
        }else {
            $item-> item_code = $category_code . '000';
        }
        $item->name = request('name');
        $item->initial_price = request('initial_price');
        $item->sale_price = request('sale_price');
        $item->quantity = request('quantity');
        $item->save();
        return redirect()->route('item.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = DB::table('categories')
                    ->where('user_id','=',Auth::id())
                    ->select('*')
                     ->get();
        $item = Item::find($id);
        return view('item.edit', compact('item','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $item = Item::find($id);

        // $item -> user_id = request('user_id');
        $item-> category_id=request('category_id');
        $item-> item_code=request('item_code');
        $item-> name=request('name');
        // $item-> initial_price=request('initial_price');
        $item-> sale_price=request('sale_price');
        $item-> quantity=request('quantity');

        $item->save();


         return redirect()->route('item.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::find($id);
        $item -> delete();

        return redirect()->route('item.index');
    }
}
