<?php

namespace App\Http\Controllers;

use App\SalePerson;
use App\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\User;


class SalePersonController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:Manager')->except('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $salePeople = SalePerson::all();
        return view ('sales_people.index', compact('salePeople'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $salePeople = SalePerson::all();
        $shops = Shop::all();
        return view('sales_people.create', compact('salePeople', 'shops'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           "name" => "required",
            "email" => "required | unique:users",
            "phone" => "required",
            "address" => "required",
            "national_id_no" => "required"
        ]);

        $user = New User;

        $user -> name = request('name');
        $user -> email = request('email');
        $user -> password = Hash::make('1234567890');

        $user -> save();

        $user->assignRole('Sales');


        $salesPerson = new SalePerson();

        $salesPerson -> user_id = $user->id;
        $salesPerson -> shop_id = request('shop_id');
        $salesPerson -> phone = request('phone');
        $salesPerson -> address = request('address');
        $salesPerson -> national_id_no = request('national_id_no');

        $salesPerson -> save();

        return redirect()->route('sales_people.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SalePerson  $salePerson
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('sales_people.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SalePerson  $salePerson
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('sales_people.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SalePerson  $salePerson
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = User::where('id', $id)->update([
            'name' => $request->name,
            'email' => $request->email
        ]);

        $salePerson = SalePerson::where('user_id', $id)->update([
            'phone' => $request->phone,
            'address' => $request->address,
            'national_id_no' => $request->national_id_no
        ]);

        return redirect()->route('sales_people.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SalePerson  $salePerson
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SalePerson::where('user_id',$id)->delete();
        User::find($id)->delete();

        return redirect()->route('sales_people.index');
    }
}
