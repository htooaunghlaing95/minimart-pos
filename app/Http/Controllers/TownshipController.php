<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Township;

class TownshipController extends Controller
{
    public function index()
    {
        $township = Township::all();
        return view ('townships.index', compact('township'));
    }

    public function create()
    {
        return view('townships.create');
    }

    public function store(Request $request)
    {
        //dd($request);
        $request->validate([
            "name" => "required",
        ]);
        $township = New Township;
        $township->name = request('name');
        $township -> save();

        return redirect()->route('townships.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $township = Township::find($id);
        return view('townships.edit', compact('township'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            "name" => 'required | min:5 | max: 100'
        ]);

        $township = Township::find($id);

        $township->name = request('name');

        $township -> save();

        return redirect()->route('townships.index');
    }

    public function destroy($id)
    {
        $township = Township::find($id);
        $township -> delete();

        return redirect()->route('townships.index');
    }
}
