<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Manager extends Model
{
    use SoftDeletes;



    protected $dates = ['deleted_at'];

    protected $fillable = ['phone', 'address'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

     public function shop()
    {
        return $this->belongsTo('App\Shop');

    }
}
